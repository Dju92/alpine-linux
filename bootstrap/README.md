# Alpine Linux Bootstrap for vps
_Should work everywhere else. If you have access to a console that can mount your volumes, and run a shell script._

A simple script that can be executed in recovery mode to bootstrap Alpine Linux on a vps server.

You can choose to install it in UEFI,or LEGACY bios.

## Creating a filesystem

This script assumes your VPS will have **three** block devices/partitions for boot, root and swap.<br>
You need to make the 3 partitions :

* LEGACY MODE

| **Name**  | **Mountpoint** | **Minimum recommended** | **Filesystem** |
|-------|------------|---------------------|------------|
| /boot | /dev/sda1   | 128 MB              | ext4       |
| /     | /dev/sda3   | 5 GB                | ext4       |
| swap  | /dev/sda2   | 512 MB              | swap       |

* EFI MODE

| **Name**  | **Mountpoint** | **Minimum recommended** | **Filesystem** |
|-----------|-------------|---------------------|------------|
| /boot/efi | /dev/sda1   | 64 M B              | fat32      |
| /         | /dev/sda3   | 5 GB                | ext4       |
| swap      | /dev/sda2   | 512 MB              | swap       |

_Name is the mountpoint in the OS, Mountpoint is the mountpoints in the control panel._

Here is an example of how to make the partitions for LEGACY mode:

```
parted /dev/sda
mklabel msdos
mkpart primary 1MB 384MB
mkpart primary linux-swap 384MB 896MB
mkpart primary 896MB 100%
toggle 1 boot
exit
```

and for the EFI mode:

```
parted /dev/sda
mklabel gpt
mkpart fat32 1MB 64MB
toggle 1 esp
mkpart linux-swap 64MB 1024MB
mkpart ext4 1024MB 100%
exit
```

Or you can also use the **[make_partitions.sh](https://gitlab.com/Dju92/alpine-linux/-/blob/main/bootstrap/make_partitions.sh?ref_type=heads)** script (edit it before, to match your drive/partitions).<br>
Default partitionning in legacy
```
./make_partitions.sh
```
for efi :
```
./make_partitions.sh efi
```

The alpine-bootstrap.sh script will make sure the partitions exist and format them in ext4 (or fat32/ext4 for efi)


## Configuration profile

### Label and notes

**Label**: Alpine

### Boot Settings

- **Bios**: legacy, or EFI
- **Boot Loader**: Syslinux (legacy) or Grub (EFI)
- **Kernel**: linux-lts


### Block Device Assignment

+ /dev/sda1
  - boot (or /boot/efi in efi mode)
+ /dev/sda2
  - swap
+ /dev/sda3
  - root

### Filesystem/Boot helpers

Turn everything off.

Boot your VPS into recovery mode with the disks assigned as above.

## Executing the Script

Connect to the VPS with VNC or via SSH or the browser console. 

You'll need curl for the script to run

    apt install curl

Then to download the script, edit it (the setup part) to reflect your partitions and network settings (dhcp or static)

```
curl -o alpine-bootstrap.sh https://gitlab.com/Dju92/alpine-linux/-/raw/main/bootstrap/alpine-bootstrap.sh?ref_type=heads
nano alpine-bootstrap.sh
chmod +x alpine-bootstrap.sh
./alpine-bootstrap.sh
```

Once that finishes, shut the vps down from recovery mode and boot it from disk.
  
After a bit, you should be at the Alpine login screen. The root user, by default, does not have a password. At this point, you can install an SSH server, which pretty much completes the installation.

## Credit

This script is basically [Andy Leap's guide](https://github.com/andyleap/docs/blob/master/docs/tools-reference/custom-kernels-distros/install-alpine-linux-on-your-linode.md) written as a working script (with fixed networking!), so big thanks to him.

Jason Chen for original [script](https://github.com/jcorme/alpine-linode-bootstrap/blob/master/README.md)

Github user [jcorme](https://github.com/jcorme) for his [version](https://github.com/jcorme/alpine-linode-bootstrap)

Then forked from [Martin Juul](https://github.com/martin-juul/alpine-linux-bootstrap)
