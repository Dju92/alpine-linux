#!/usr/bin/env sh


####### setup
#
#
# boot type: legacy of efi
BOOT_TYPE="legacy"
# drive where the bootloader is installed
SYS_DISK="/dev/vda"
# partition for /boot (legacy) or /boot/efi (efi)
BOOT_DEV="/dev/vda1"
# partition for rootfs
ROOT_DEV="/dev/vda3"
# partition for swap
SWAP_DEV="/dev/vda2"
# folder where the partitions are mounted
MOUNT_PATH="/alpine"
# hostname
HOST="alpine-linux"
# static or dhcp
NETWORK="dhcp"
# static ip
NET_IP=""
# static netmask
NET_MASK=""
# static gateway
NET_GW=""
# if empty, copy resolv.conf. otherwhise, set nameserver ${NET_DNS}
NET_DNS="10.0.2.3"
# mirror to download from
MIRROR="https://dl-cdn.alpinelinux.org/alpine"
# alpine version
ALPINE_VER=${ALPINE_VER:-"latest-stable"}
# apk tools version to fetch the apk-tools-static
APK_TOOLS_VER=${APK_TOOLS_VER:-"2.14.6-r3"}
# which linux kernel to install (lts, virt, edge)
KERNEL="virt"
#
#
####### end setup


if [ ! -b $BOOT_DEV ]; then
	echo "Error, ${BOOT_TYPE} device $BOOT_DEV does not exist !"
	exit 1
fi
if [ ! -b $ROOT_DEV ]; then
	echo "Error, root device $ROOT_DEV does not exist !"
	exit 2
fi
if [ ! -b $SWAP_DEV ]; then
	echo "Error, swap device $SWAP_DEV does not exist !"
	exit 3
fi

PATH=${PATH}:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin

CMDS_NEEDED="mkfs.ext4 mkswap chroot"
if [ "$BOOT_TYPE" = "efi" ]; then
	CMDS_NEEDED="${CMDS_NEEDED} mkfs.fat"
fi
for cmd in $CMDS_NEEDED; do
	CMD1=$(command -v "${cmd}")
	CMD2=$(type "$cmd" > /dev/null)
	if [ -z "$CMD1" ] && [ -z "$CMD2" ]; then
		echo "> Error, $cmd command not found. please install it and restart this script"
		exit 4
	fi
done

CMDS_GET="wget curl"
CMD_GET=""
for cmd in $CMDS_GET; do
	CMD1=$(command -v "${cmd}")
	CMD2=$(type "$cmd" > /dev/null)
	if [ -n "$CMD1" ] || [ -n "$CMD2" ]; then
		CMD_GET=$cmd
		break
	fi
done
if [ -z "$CMD_GET" ]; then
	echo "> Error, curl and wget unavailable. please intall one of them"
	exit 6
fi

PATH_EFI="/sys/firmware/efi"
if [ -d "$PATH_EFI" ] && [ "$BOOT_TYPE" = "legacy" ]; then
	echo "> WARNING: you asked for a legacy boot installation, but it seems this system is running in EFI boot mode."
	echo "Are you sure you want to continue ? (y/n)"
	read -r _cont
	[ "$_cont" != "y" ] && exit
fi
if [ ! -d "$PATH_EFI" ] && [ "$BOOT_TYPE" = "efi" ]; then
	echo "> WARNING: you asked for a EFI boot installation, but it seems this system is running in legacy boot mode."
	echo "Are you sure you want to continue ? (y/n)"
	read -r _cont
	[ "$_cont" != "y" ] && exit
fi

if [ "$NETWORK" = "dhcp" ]; then
INTERFACES="auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp
"
elif [ "$NETWORK" = "static" ]; then
INTERFACES="auto lo
iface lo inet loopback
auto eth0
iface eth0 inet static
  address ${NET_IP}
  netmask ${NET_MASK}
  gateway ${NET_GW}
"
else
	echo "Error, NETWORK should be 'static' or 'dhcp'"
	exit 5
fi

ARCH=$(uname -m)

echo "> MAKING FILESYSTEMS ... "
if [ "$BOOT_TYPE" = "efi" ]; then
	mkfs.fat -F32 $BOOT_DEV
elif [ "$BOOT_TYPE" = "legacy" ]; then
	mkfs.ext4 $BOOT_DEV
fi
mkfs.ext4 $ROOT_DEV
mkswap $SWAP_DEV

echo "> MOUNTING FILESYSTEMS ... "
PWD_ORG=$(pwd)
mkdir $MOUNT_PATH
mount -t ext4 $ROOT_DEV $MOUNT_PATH
cd $MOUNT_PATH
mkdir boot
if [ "$BOOT_TYPE" = "efi" ]; then
	mkdir boot/efi
	mount -t vfat $BOOT_DEV $MOUNT_PATH/boot/efi
elif [ "$BOOT_TYPE" = "legacy" ]; then
	mount -t ext4 $BOOT_DEV $MOUNT_PATH/boot
fi

echo "> BOOTSTRAPING ... "
if [ "$CMD_GET" = "curl" ]; then
	curl -s $MIRROR/$ALPINE_VER/main/$ARCH/apk-tools-static-${APK_TOOLS_VER}.apk | tar xz
elif [ "$CMD_GET" = "wget" ]; then
	wget -O- $MIRROR/$ALPINE_VER/main/$ARCH/apk-tools-static-${APK_TOOLS_VER}.apk | tar xz
fi
./sbin/apk.static --repository $MIRROR/$ALPINE_VER/main/ --update-cache --allow-untrusted --root $MOUNT_PATH --initdb add alpine-base

echo "> FSTAB... "

if [ "$BOOT_TYPE" = "efi" ]; then

cat <<EOF > ${MOUNT_PATH}/etc/fstab
$ROOT_DEV    /   ext4    defaults,noatime    0   0
$BOOT_DEV    /boot/efi   vfat    defaults    0  1
$SWAP_DEV    swap    swap    defaults    0   0
EOF

elif [ "$BOOT_TYPE" = "legacy" ]; then

cat <<EOF > ${MOUNT_PATH}/etc/fstab
$ROOT_DEV    /   ext4    defaults,noatime    0   0
$BOOT_DEV    /boot   ext4    defaults,noatime    0  1
$SWAP_DEV    swap    swap    defaults    0   0
EOF

fi

echo "> INITTAB ... "
cat <<EOF > ${MOUNT_PATH}/etc/inittab
# /etc/inittab
::sysinit:/sbin/openrc sysinit
::sysinit:/sbin/openrc boot
::wait:/sbin/openrc default

# setup a couple of gettys
tty1::respawn:/sbin/getty 38400 tty1
tty2::respawn:/sbin/getty 38400 tty2
tty3::respawn:/sbin/getty 38400 tty3
# Put a getty on the serial port
#ttyS0::respawn:/sbin/getty -L ttyS0 115200 vt100

# Stuff to do for the 3-finger salute
::ctrlaltdel:/sbin/reboot

# Stuff to do before rebooting
::shutdown:/sbin/openrc shutdown
EOF
echo ttyS0 >> $MOUNT_PATH/etc/securetty

echo "> RESOLV.CONF ... "
if [ -z "$NET_DNS" ]; then
	cp /etc/resolv.conf $MOUNT_PATH/etc
else
	echo "nameserver ${NET_DNS}" > $MOUNT_PATH/etc/resolv.conf
fi

echo "> MOUNTING PROC/SYS/DEV ... "
mount -t proc none $MOUNT_PATH/proc
mount --bind /sys $MOUNT_PATH/sys
mount --bind /dev $MOUNT_PATH/dev

echo "> CHROOT & FINAL SETUP"
LANG=C chroot $MOUNT_PATH /bin/sh<<CHROOT1
setup-apkrepos -1
apk update
apk add -q linux-firmware-none linux-$KERNEL vim nano bash bash-completion acpi mkinitfs openssh
apk add -q util-linux pciutils coreutils binutils findutils grep iproute2 procps psmisc e2fsprogs
setup-hostname -n $HOST
echo "127.0.1.1 ${HOST}" >> /etc/hosts
printf "$INTERFACES" | setup-interfaces -i
sed -i -e "s:^features=.*:features=\"ata base ide scsi usb virtio ext4\":" /etc/mkinitfs/mkinitfs.conf
rc-update add acpid default
rc-update add sshd default
rc-update add bootmisc boot
rc-update add crond default
rc-update add devfs sysinit
rc-update add dmesg sysinit
rc-update add hostname boot
rc-update add hwclock boot
rc-update add hwdrivers sysinit
rc-update add killprocs shutdown
rc-update add mdev sysinit
rc-update add modules boot
rc-update add mount-ro shutdown
rc-update add networking boot
rc-update add savecache shutdown
rc-update add seedrng boot
rc-update add swap boot
CHROOT1

echo "> BOOTLOADER ... "

if [ "$BOOT_TYPE" = "legacy" ]; then

LANG=C chroot $MOUNT_PATH /bin/sh<<CHROOT2
apk add syslinux
dd bs=440 count=1 conv=notrunc if=/usr/share/syslinux/mbr.bin of=${SYS_DISK}
sed -i -e "s:^root=.*:root=${ROOT_DEV}:" \
 -e "s:^default_kernel_opts=.*:default_kernel_opts=\"quiet rootfstype=ext4\":" \
 -e "s:^modules=.*:modules=sd-mod,usb-storage,ext4:" \
 /etc/update-extlinux.conf
extlinux --install /boot
update-extlinux
CHROOT2

if [ "$KERNEL" != "lts" ]; then
LANG=C chroot $MOUNT_PATH /bin/sh<<CHROOT3
sed -i -e "s:^default=.*:default=${KERNEL}:" \
/etc/update-extlinux.conf
update-extlinux
CHROOT3
fi

umount $MOUNT_PATH/boot

elif [ "$BOOT_TYPE" = "efi" ]; then

[ -d "$PATH_EFI/efivars" ] && mount -o bind $PATH_EFI/efivars ${MOUNT_PATH}${PATH_EFI}/efivars
LANG=C chroot $MOUNT_PATH /bin/sh<<CHROOT2
apk add grub-efi efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=alpine
echo "GRUB_CMDLINE_LINUX_DEFAULT=\"quiet rootfstype=ext4 modules=sd-mod,usb-storage,ext4\"" >> /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
CHROOT2
umount ${MOUNT_PATH}${PATH_EFI}/efivars
umount $MOUNT_PATH/boot/efi

fi

echo "> SETUP FINISHED ! CLEANING & UNMOUTING..."
cd $PWD_ORG
rm $MOUNT_PATH/.PKGINFO
rm $MOUNT_PATH/.SIGN.RSA.alpine*
rm $MOUNT_PATH/sbin/apk.static.SIGN.*
umount $MOUNT_PATH/proc
umount $MOUNT_PATH/sys
umount $MOUNT_PATH/dev
umount $MOUNT_PATH

echo ""
echo "> DONT FORGET TO SET THE ROOT PASSWORD AFTER REBOOT !"
echo "> "
echo "> if needed, use the 'setup-keymap' command to setup your keyboard mapping"
echo "> and to setup your timezone, use the 'setup-timezone' command."
echo "> "
echo "> Now, reboot and anjoy :)"
