#!/usr/bin/env sh

# /!\
#
# partitions to install for bootstrapping alpine linux on a vm/vps
#
# Be sure to indicate the drive on which install the partitions !
#
# /!\

DRIVE="/dev/vda"

if [ "$1" = "legacy" ] || [ "$1" = "efi" ]; then
	boot_type=$1
else
	echo "you must call this script with efi or legacy in argument"
	exit 1
fi

CMD1=$(which parted)
CMD2=$(command -v parted)
if [ -z "$CMD1" ] && [ -z "$CMD2" ]; then
	echo "parted command needed, please intall it."
	exit 2
fi

if [ "$boot_type" = "legacy" ]; then

parted ${DRIVE}<<EOF
mklabel msdos
mkpart p 1MB 512MB
toggle 1 boot
mkpart p linux-swap 512MB 1024MB
mkpart p 1024MB 100%
EOF

elif [ "$boot_type" = "efi" ]; then

parted ${DRIVE}<<EOF
mklabel gpt
mkpart boot fat32 1MB 64MB
toggle 1 esp
mkpart swap linux-swap 64MB 1024MB
mkpart root ext4 1024MB 100%
EOF

fi

